
#include <FastLED.h>

// MPG Wheel Setup
#define PIN_A 2
#define PIN_B 3

// MPG Wheel state
volatile int count = 0;
volatile int pulse_count = 0;
volatile byte pin_a_state;
volatile byte pin_b_state;
volatile byte cw;

// Change Detection
int last_count = 0;

#define NUM_LEDS 16
#define LED_PIN 4
#define LED_OUPUT
CRGB leds[NUM_LEDS];

void setup() {
  // Configure MPG pins
  pinMode(PIN_A, INPUT);
  pinMode(PIN_B, INPUT);

  // Configure interrupts
  attachInterrupt(digitalPinToInterrupt(PIN_A), pin_aChange, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_B), pin_bChange, CHANGE);

  // Configure LEDS
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(42);
  leds[0]  = CRGB::Blue;
  FastLED.show();
  // Set initial state.
  pin_a_state = digitalRead(PIN_A);
  pin_b_state = digitalRead(PIN_B);
  cw = HIGH;
  count = 0;
  last_count = 0;
  pulse_count = 0;

  Serial.begin(115200);
}


void loop() {
  if (last_count != pulse_count) {
    // Get the LED to light. Ensure that the number is positive
    int active = (-pulse_count % NUM_LEDS + NUM_LEDS) % NUM_LEDS;
    int steps = pulse_count - last_count;
    pulse_count = pulse_count % NUM_LEDS;
    last_count = pulse_count;
    for (int i = 0; i < NUM_LEDS; i++) {
      if (i == active) {
        if (cw == HIGH) {
          leds[i] = CRGB::Red;
        } else {
          leds[i] = CRGB::Green;
        }
      }  else {
        leds[i] = CRGB::Black;
      }
    }

    FastLED.show();
    Serial.println(steps);

  }
}

// inline updateState since we really don't want too may function calles inside the interrupt service routines
inline void updateState() __attribute__((always_inline));

inline void updateState() {
  // The MPG wheel has 100 detents, and always has lines PIN_A and PIN_B high at a detent.
  // There are 4 transitions from detent to detent.
  if (cw) {
    count += 1;
    if (count >= 4) {
      pulse_count += 1;
    }
  } else {
    count -= 1;
    if (count <= -4) {
      pulse_count -= 1;
    }
  }

  // Ensure that count == 0 at detents.
  if (pin_a_state & pin_b_state) {
    count = 0;
  }
}

/* Manage if PIN_A changes.
    If PIN_A transitions, and its new state is the complement of PIN_B the dial has rotated clockwise.
    Conversely, if they're the same the dial has rotated counter clockwise.
*/
void pin_aChange() {
  pin_a_state = digitalRead(PIN_A);
  pin_b_state = digitalRead(PIN_B);
  cw = pin_a_state ^ pin_b_state;
  updateState();
}

/* Manage if PIN_B changes
    If PIN_BB transitions, and its new state is the same as PIN_A the dial has rotated clockwise.
    Conversely, if they're the different the dial has rotated counter clockwise.
*/
void pin_bChange() {
  pin_a_state = digitalRead(PIN_A);
  pin_b_state = digitalRead(PIN_B);
  cw = !(pin_a_state ^ pin_b_state);
  updateState();
}
